let university = {
  credits: {
    frontEnd: [4, 7],
    backEnd: [6, 3],
  },
  gpa: {
    0.5: 0.5,
    1: 1,
    2: 2,
    3: 3,
    4: 4,
  },
  students: [],
};

// students = [];

university.students[0] = {
  firstName: "Jean",
  lastName: "Reno",
  age: 26,
  scores: {
    frontEnd: [62, 57],
    backEnd: [88, 90],
  },
};

university.students[1] = {
  firstName: "Claud",
  lastName: "Monet",
  age: 19,
  scores: {
    frontEnd: [77, 52],
    backEnd: [92, 67],
  },
};

university.students[2] = {
  firstName: "Van",
  lastName: "Gogh",
  age: 21,

  scores: {
    frontEnd: [51, 98],
    backEnd: [65, 70],
  },
};

university.students[3] = {
  firstName: "Dam",
  lastName: "Squares",
  age: 36,

  scores: {
    frontEnd: [82, 53],
    backEnd: [80, 65],
  },
};

console.log(university);

//Calculating Total Average and GPA

for (let id = 0; id < university.students.length; id++) {
  university.students[id].sumOfScoures = 0;
  university.students[id].GPA = 0;

  university.students[id].sumOfCredits = 0;

  //Calculating Total
  for (
    let score = 0;
    score < university.students[id].scores.frontEnd.length;
    score++
  ) {
    university.students[id].sumOfScoures +=
      university.students[id].scores.frontEnd[score];
    university.students[id].sumOfCredits += university.credits.frontEnd[score];
  }
  for (
    let score = 0;
    score < university.students[id].scores.backEnd.length;
    score++
  ) {
    university.students[id].sumOfScoures +=
      university.students[id].scores.backEnd[score];
    university.students[id].sumOfCredits += university.credits.backEnd[score];
  }

  //Calculation avearage
  university.students[id].averageScore =
    university.students[id].sumOfScoures /
    (university.students[id].scores.frontEnd.length +
      university.students[id].scores.backEnd.length);
}

for (let id = 0; id < university.students.length; id++) {
  university.students[id].GPA = 0;
  for (let i = 0; i < university.students[id].scores.frontEnd.length; i++) {
    if (university.students[id].scores.frontEnd[i] >= 91) {
      university.students[id].GPA +=
        university.credits.frontEnd[i] * university.gpa[4];
    } else if (university.students[id].scores.frontEnd[i] >= 81) {
      university.students[id].GPA +=
        university.credits.frontEnd[i] * university.gpa[3];
    } else if (university.students[id].scores.frontEnd[i] >= 71) {
      university.students[id].GPA +=
        university.credits.frontEnd[i] * university.gpa[2];
    } else if (university.students[id].scores.frontEnd[i] >= 61) {
      university.students[id].GPA +=
        university.credits.frontEnd[i] * university.gpa[1];
    } else {
      university.students[id].GPA +=
        university.credits.frontEnd[i] * university.gpa[0.5];
    }
  }

  for (
    let index = 0;
    index < university.students[id].scores.backEnd.length;
    index++
  ) {
    if (university.students[id].scores.backEnd[index] >= 91) {
      university.students[id].GPA +=
        university.credits.backEnd[index] * university.gpa[4];
    } else if (university.students[id].scores.backEnd[index] >= 81) {
      university.students[id].GPA +=
        university.credits.backEnd[index] * university.gpa[3];
    } else if (university.students[id].scores.backEnd[index] >= 71) {
      university.students[id].GPA +=
        university.credits.backEnd[index] * university.gpa[2];
    } else if (university.students[id].scores.backEnd[index] >= 61) {
      university.students[id].GPA +=
        university.credits.backEnd[index] * university.gpa[1];
    } else {
      university.students[id].GPA +=
        university.credits.backEnd[index] * university.gpa[0.5];
    }
  }

  university.students[id].GPA /= university.students[id].sumOfCredits;
}

//Calculating Total Average

university["totalAverage"] = 0;

for (let id = 0; id < university.students.length; id++) {
  university["totalAverage"] +=
    university.students[id].averageScore / university.students.length;
}

// 3. გამოთვალეთ 4 - ვე სტუდენტის საშუალო არითმეტიკული (ანუ ვაფშე ყველა ქულა და ყველა საგანი),
// სტუდენტებს, რომელთა ცალკე საშუალო არითმეტიკული მეტი აქვთ ვიდრე საერთო არითმეტიკული, მივანიჭოთ
// "წითელი დიპლომის მქონეს" სტატუსი, ხოლო ვისაც საერთო საშუალოზე ნაკლები აქვს - "ვრაგ ნაროდა" სტატუსი

let i = 0;

while (university.students.length > i) {
  university.students[i].averageScore < university.totalAverage
    ? (university.students[i]["statusOfDiploma"] = "Enemy of the people")
    : (university.students[i]["statusOfDiploma"] = "with Honours");
  i++;
}

//4 უნდა გამოავლინოთ საუკეთესო სტუდენტი GPA ს მიხედვით

let id = 1;
let highestGPAStudent = university.students[0];
while (id < university.students.length) {
  if (university.students[id].GPA > highestGPAStudent.GPA) {
    highestGPAStudent = university.students[id];
  }
  id++;
}

console.log(highestGPAStudent);

//5. უნდა გამოავლინოთ საუკეთესო სტუდენტი 21 + ასაკში საშუალო ქულების მიხედვით
//(აქ 21+ იანის ხელით ამორჩევა არ იგულისხმება, უნდა შეამოწმოთ როგორც საშუალო ქულა, ასევე ასაკი)

let studentScoresOver21 = [];

for (let id = 0; id < university.students.length; id++) {
  if (university.students[id].age >= 21) {
    studentScoresOver21[id] = university.students[id].averageScore;
  } else {
    studentScoresOver21[id] = 0;
  }
}

let highestScoreover21 = studentScoresOver21[0];
let studentHighestOve21 = university.students[0];

for (let index = 0; index < studentScoresOver21.length; index++) {
  if (studentScoresOver21[index] > highestScoreover21) {
    highestScoreover21 = studentScoresOver21[index];
    studentHighestOve21 = university.students[index];
  }
}

console.log(studentHighestOve21);

//6. უნდა გამოავლინოთ სტუდენტი რომელიც საუკეთესოა ფრონტ-ენდის საგნებში საშუალო ქულების მიხედვით (js, react)

let frontEndArrey = [];

for (id = 0; id < university.students.length; id++) {
  let sumOfFrontEnd = 0;
  for (
    let subjact = 0;
    subjact < university.students[id].scores.frontEnd.length;
    subjact++
  ) {
    sumOfFrontEnd += university.students[id].scores.frontEnd[subjact];
  }
  frontEndArrey[id] = sumOfFrontEnd;
}

let frontEndKing = frontEndArrey[0];
let frontEndStudent = university.students[0];

for (let i = 0; i < frontEndArrey.length; i++) {
  if (frontEndArrey[i] > frontEndKing) {
    frontEndKing = frontEndArrey[i];
    frontEndStudent = university.students[i];
  }
}

console.log(frontEndStudent);

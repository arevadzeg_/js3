tourists = [];

tourists[0] = {
  name: "Mark",
  age: 19,
  cities: ["tbilisi", "london", "rome", "berlin"],
  costs: [120, 200, 150, 140],
};

tourists[1] = {
  name: "Bob",
  age: 21,
  cities: ["miami", "moscow", "vienna", "riga", "kiev"],
  costs: [90, 240, 100, 76, 123],
};

tourists[2] = {
  name: "Sam",
  age: 22,
  cities: ["tbilisi", "budapest", "warsaw", "vilnius"],
  costs: [118, 95, 210, 236],
};

tourists[3] = {
  name: "Anna",
  age: 20,
  cities: ["new-york", "athen", "sydney", "tokyo"],
  costs: [100, 240, 50, 190],
};

tourists[4] = {
  name: "Alex",
  age: 23,
  cities: ["paris", "tbilisi", "madrid", "minsk"],
  costs: [96, 134, 76, 210, 158],
};

// 2. გაარკვიეთ თითოეული სტუდენტისთვის არის თუ არა ის ზრდასრული (21+, ზრდასრულში 21 წელიც იგუისხმება),
// პასუხის მიხედვით შექმენით შესაბამისი key და შეინახეთ შესაბამისი სტუდენტის ინფოში როგორც boolean ტიპის ცვლადი.

for (let i = 0; i < tourists.length; i++) {
  tourists[i]["over21"] = tourists[i].age >= 21;
}

// 3. გაარკვიეთ თითოეული სტუდენტისთვის არის თუ არა ის ნამყოფი საქართველოში და პასუხის
// მიხედვით წინა დავალების ანალოგიურად შექმენით შესაბამისი key და შეინახეთ შესაბამისი
// სტუდენტის ინფოში ამჯერად რაიმე სტინგის სახით (ნამყოფია, არაა ნამყოფი და ა.შ.).

for (let id = 0; id < tourists.length; id++) {
  tourists[id]["visitedTbilisi"] = "Have not Visited Tbilisi";
  for (let city = 0; city < tourists[id].cities.length; city++) {
    if (tourists[id].cities[city] == "tbilisi") {
      tourists[id]["visitedTbilisi"] = "Visited Tbilisi";
    }
  }
}

// 4. დაითვალეთ თითოეული ტურისტისთვის ჯამში რა თანხა დახარჯა მოგზაურობისას.

for (let id = 0; id < tourists.length; id++) {
  let totalCost = 0;
  for (let cost = 0; cost < tourists[id].costs.length; cost++) {
    totalCost += tourists[id].costs[cost];
  }
  tourists[id]["totalCost"] = totalCost;
}

// 5. დაითვალეთ თითოეული ტურისტისთვის საშუალოდ რა თანხა დახარჯა მოგზაურობისას.

let index = 0;
while (index < tourists.length) {
  tourists[index]["averageCost"] =
    tourists[index].totalCost / tourists[index].costs.length;
  index++;
}

console.log(tourists);

// 6. გამოავლინეთ ყველაზე "მხარჯველი" ტურისტი, დალოგეთ ვინ დახარჯა ყველაზე მეტი და რამდენი.

let spender = 0;

let biggestSpender = tourists[0];

while (spender < tourists.length) {
  biggestSpender =
    tourists[spender].totalCost > biggestSpender.totalCost
      ? tourists[spender]
      : biggestSpender;
  spender++;
}
console.log(biggestSpender);
